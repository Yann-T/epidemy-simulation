#!/usr/bin/gnuplot
set datafile separator ","
set style data lines
set terminal wxt 1 noraise
set grid
set title "Plague simulation (Operating System Project)" font "DejaVuSerif, 18"
set xlabel "Rounds"
set ylabel "Citizens"
plot "misc/gnuplot/evolution.txt" u 1:2 t "Healthy" lw 2 lc rgb "#7bc345",\
"misc/gnuplot/evolution.txt" u 1:3 t "Sick" lw 2 lc rgb "#df924e",\
"misc/gnuplot/evolution.txt" u 1:4 t "Dead" lw 2 lc rgb "#eb3f3f",\
"misc/gnuplot/evolution.txt" u 1:5 t "Burned" lw 2 lc rgb "#51646b"
pause 1
refresh
reread