# Epidemic simulation project

## Description

This project was made for the operating systems course at ENSICAEN.

For this project we had to create the simulation of an epidemy in a randomly generated city represented by tiles with various properties (contamination level, type of building in the tile ...).
In this city there are citizen who can have different jobs : firefighters who help against the contamination, doctors who cure people from diseases caused by contamination and reporters who are in charge of creating news reports about the state of the city and its citizens.

This program can be easily customized using the file inc/epidemy_const.h


## Commands and usage

### Building the project 

`make`
Creates all the executables of the project

`make clean`
Clean the project's directory

`make distclean`
Clean the project's directory as well as deleting the binaries (should also be used when before building the project after having changed a header file).

### Executing the programs

`./bin/epidemic_sim`
(from the root of the project) Execute the simulation

`./bin/press_agency`
Execute the press agency (in order to see the messages sent by the reporters)

### Dependencies

This project work best with ncurses installed (To use the ncurses display you need to set the NCURSES_DISPLAY macro to 1 in the file inc/epidemy_const.h), it is also recommended to have gnuplot installed in order to have a graph with the evolution of the population state in real time

## Members of the project

- CARIOU Erwan
- SAMUTH Benjamin
- TRUPOT Yann