/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file epidemic_sim_exec.h
 */

#ifndef __EPIDEMIC_SIM_EXEC_H
#define __EPIDEMIC_SIM_EXEC_H

#include <sys/types.h>

/**
 * Execute a program
 * @param path the path of the executable
 * @param args the argument of the executable
 * @return the pid of the executable
 */
pid_t start_exec(const char * path, const char * args);

/**
 * Setup an action to do when a certain signal comes
 * @param sig the signal type
 * @param process the function to execute
 */
void setup_on_signal(int sig, void (*process)(int));

/**
 * Process a turn
 */
void process_new_turn();

/**
 * Start the timer by executing the timer program
 */
void start_timer();

/**
 * Start the citizen manager by executing the citizen_manager program
 */
void start_citizen_manager();

/**
 * Start the game
 */
void start_game();

/**
 * End the game
 */
void end_game();

#endif
