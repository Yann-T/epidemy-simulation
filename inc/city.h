/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file city.h
 */

#ifndef __CITY_H_
#define __CITY_H_

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <math.h>

#include "epidemy_const.h"

/**
 * Defines the different types of buildings
 */
typedef enum _building_type
{
	WASTELAND,
	HOUSE,
	HOSPITAL,
	FIRESTATION
} Building_type;

/**
 * Defines the different types of citizens
 */
typedef enum _citizen_Type {
	NORMAL,
	FIREFIGHTER,
	DOCTOR,
	REPORTER,
	DEAD,
	BURNED
} Citizen_type;


/**
 * Defines the status of a citizen
 * @param x The x coordinate in the city
 * @param y The y coordinate int the city
 * @param contaminationLevel The level of contamination between 0 and 1
 * @param isSick A boolean to say if the citizen is sick
 * @param name the name of the citizen
 * @param sicknessDuration The number of day(s) the citizen have been sick
 * @param type The type of citizen
 * @param days_spend_in_hospital_healthy the number of days spent in the hospital while not being sick
 */
typedef struct status {
	unsigned int x, y;
	double contamination;
	int is_sick;
	char name[CITIZEN_NAME_MAX_LENGHT];
	int sickness_duration;
	Citizen_type type;
	int days_spent_in_hospital_healthy;
} Status;

/**
 * Represents a tile of the map
 * @param x The x coordinate in the city
 * @param y The y coordinate int the city
 * @param capacity The max number of citizens on this tile
 * @param nb_citizens The number of citizens on the tile
 * @param type the type of the building on the tile
 * @param contamination the contamination level on the tile
 */
typedef struct tile {
	unsigned int x;
	unsigned int y;
	int capacity;
	int nb_citizens;
	Building_type type;
	double contamination;
} Tile;

/**
 * Represents the city in which the epidemy occurs
 * @param map A 2D array of tiles representing the city
 * @param citizens An array of Status representing the citizens
 */
typedef struct city {
	Tile map[CITY_WIDTH][CITY_HEIGHT];
	Status citizens[NB_TOTAL];
} City;

/**
 * Initialize a tile
 * @param city the city
 * @param x The x coordinate in the city
 * @param y The y coordinate int the city
 * @param building the building type of the tile
 * @param capacity the capacity
 * @param contamination the contamination level of the tile
 */
void init_building(City * city, int x, int y, Building_type building, int capacity, double contamination);

/**
 * Initializes a city
 * @param city the city
 */
void init_city(City* city);

/**
 * Gives the number of citizens alive on a tile
 * @param city the city
 * @param tile the tile
 * @return the number of citizens alive on the tile
 */
int get_number_of_living_citizen_on_tile(City city, Tile tile);

/**
 * Add more buildings to the map (only if the bonus rule is enable in epidemy_const.h)
 * @param city The city
 */
void init_bonus_buildings(City* city);

#endif
