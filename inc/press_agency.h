/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file press_agency.h
 */

#ifndef __PRESS_AGENCY_H
#define __PRESS_AGENCY_H

/**
 * Open a message queue
 * @return the message queue descriptor
 */
mqd_t open_mqueue();

/**
 * Handle a message
 * @param message the message
 * @param priority the priority level of the message
 */
void handle_message(char * message, unsigned int priority);

#endif
