/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file thread_manager.h
 */

#ifndef __CITIZEN_H
#define __CITIZEN_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <math.h>

#include "city.h"

/**
 * Used to pass the city and the citizen as arguments in a thread
 * @param city the city
 * @param status the status of the citizen
 */
typedef struct citizen_data {
	City * city;
	Status * status;
} Citizen_data;

/**
 * Creates a citizen of the given type and launches his thread.
 * @param status the status of the citizen in the shared memory
 * @param type the type of the citizen
 * @param x the initial x coordinate of the citizen
 * @param y the initial y coordinate of the citizen
 * @param city the city
 * @param number_names the number of names in the file
 * @param file the file containing the possible names for the citizen
 * @return The pthread_t of the started thread.
 */
pthread_t create_citizen(Status * status, Citizen_type type, int x, int y, City * city, int number_names, FILE* file);

/**
 * Initializes a citizen with default values at the given coordinates
 * @param data the data used by the thread to be filled
 * @param type the type of the citizen
 * @param x the initial x coordinate of the citizen
 * @param y the initial y coordinate of the citizen
 * @param number_names the number of names in the file
 * @param file the file containing the possible names for the citizen
 */
void init_citizen(Citizen_data* data, Citizen_type type, int x, int y, int number_names, FILE* file);

/**
 * Creates and start citizen and their thread from the status in the city
 * @param city The city
 * @return An array containing all the thread's id
 */
pthread_t* create_population(City * city);

/**
 * Creates the thread who is in charge of notifying the others when it
 * receive a signal from the timer
 * @return pthread_t the id of the thread
 */
pthread_t create_notifier_thread();

/**
 * Handles the signal sent by the timer and notify the other threads
 */
void handle_signal();

/**
 * Initialize the signal handling
 */
void* handle_notifier();

#endif