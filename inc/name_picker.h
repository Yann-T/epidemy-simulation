/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file name_picker.h
 */

#ifndef __NAME_PICKER
#define __NAME_PICKER

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#include "epidemy_const.h"

/**
 * Returns a string containing a random name from the provided file
 * @param file the file containing the names
 * @param nb_lines the number of lines (or names) in the file
 * @return a string containing the random name
 */
char* get_random_name_from_file(FILE* file, int nb_lines);

/**
 * Returns the number of line from the given file
 * @param file the file on which we want to count the lines
 * @return the number of lines
 */
int get_number_of_line_from_file(FILE* file) ;

#endif