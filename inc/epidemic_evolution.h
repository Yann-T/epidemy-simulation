/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file epidemic_evolution.h
 */

#ifndef __EPIDEMIC_EVOLUTION
#define __EPIDEMIC_EVOLUTION

#include <stdio.h>

#include "city.h"

/**
 * Represents the data used for the statistics in the simulation
 * @param healthy the number of healthy citizens
 * @param sick the number of sick citizens
 * @param dead the number of dead citizens
 * @param burned the number of burned citizens
 */
typedef struct _stats {
	unsigned int healthy;
	unsigned int sick;
	unsigned int dead;
	unsigned int burned;
} Statistics;

/**
 * Give the current data of the city
 * @param city the city
 * @return The statitics of the city
 */
Statistics get_epidemic_evolution(City* city);

/**
 * Print statistics in a file
 * @param evolution the file
 * @param stats the statistics
 * @param turn the current turn
 */
void write_statistics(FILE * evolution, Statistics stats, int turn);
#endif
