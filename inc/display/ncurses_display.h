/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file ncurses_display.h
 */
#include <ncurses.h>
#include <string.h>

#include "epidemic_evolution.h"

/* Ncurses tiles colors */
#define WASTELAND_COLOR 1
#define HOUSE_COLOR 2
#define HOSPITAL_COLOR 3
#define FIRESTATION_COLOR 4
#define NO_CONTAMINATION_COLOR 5
#define SLIGHT_CONTAMINATION_COLOR 6
#define AVERAGE_CONTAMINATION_COLOR 7
#define HIGH_CONTAMINATION_COLOR 8
#define VERY_HIGH_CONTAMINATION_COLOR 9
#define HEALTHY_CITIZEN_TEXT_COLOR 10
#define SICK_CITIZEN_TEXT_COLOR 11
#define DEAD_CITIZEN_TEXT_COLOR 12
#define BURNED_CITIZEN_TEXT_COLOR 13

/**
 * Draw the informations on the screen
 * @param turn the number of the current turn
 * @param stats the statistics to display
 * @param city the city
 * @param offset_x the x offset of the display
 * @param offset_y the y offset of the display
 */
void draw_screen_ncurses(int turn, Statistics stats, City city, int offset_x, int offset_y);

/**
 * Display the contamination map on the top right
 * @param city the city
 * @param offset_x the x offset of the display
 * @param offset_y the y offset of the display
 */
void display_contamination_map_ncurses(City city, int offset_x, int offset_y);

/**
 * Give a color index correponding to a contamination level
 * @param contamination the contamination level
 * @return a color index
 */
int get_color_index_from_contamination_level(double contamination);

/**
 * Display the city on the top left
 * @param city the city
 * @param offset_x the x offset of the display
 * @param offset_y the y offset of the display
 */
void display_city_ncurses(City city, int offset_x, int offset_y);

/**
 * Initialize ncurses colors
 */
void init_ncurses_colors();

/**
 * Display the statistics on the bottom
 * @param stats the statistics to display
 * @param turn the number of the current turn
 * @param offset_x the x offset of the display
 * @param offset_y the y offset of the display
 */
void display_statistics_ncurses(Statistics stats, int turn, int offset_x, int offset_y);