/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file basic_display.h
 */
#include <string.h>

#include "epidemic_evolution.h"

/* colors */
#define COLOR_RESET "\x1b[0m"
#define YELLOW_BACKGROUND "\x1b[43m"
#define GRAY_BACKGROUND "\x1b[30;47m"
#define BLUE_BACKGROUND "\x1b[44m"
#define RED_BACKGROUND "\x1b[41m"
#define GREEN_BACKGROUND "\x1b[42m"
#define MAGENTA_BACKGROUND "\x1b[45m"

#define WASTELAND_COLOR YELLOW_BACKGROUND
#define HOUSE_COLOR GRAY_BACKGROUND
#define HOSPITAL_COLOR BLUE_BACKGROUND
#define FIRESTATION_COLOR RED_BACKGROUND

#define NO_CONTAMINATION_COLOR GRAY_BACKGROUND
#define SLIGHT_CONTAMINATION_COLOR GREEN_BACKGROUND
#define AVERAGE_CONTAMINATION_COLOR YELLOW_BACKGROUND
#define HIGH_CONTAMINATION_COLOR RED_BACKGROUND
#define VERY_HIGH_CONTAMINATION_COLOR MAGENTA_BACKGROUND

/**
 * Draw the informations on the screen
 * @param turn the number of the current turn
 * @param stats the statistics to display
 * @param city the city
 */
void draw_screen(int turn, Statistics stats, City city);

/**
 * Display the contamination map on the top right
 * @param city the city
 */
void display_contamination_map(City city);

/**
 * Display the city on the top left
 * @param city the city
 */
void display_city(City city);

/**
 * Display the statistics on the bottom
 * @param stats the statistics to display
 * @param turn the number of the current turn
 */
void display_statistics(Statistics stats, int turn);

/**
 * Give a color code correponding to a contamination level
 * @param contamination the contamination level
 * @return a color code
 */
char* get_color_prefix_from_contamination_level(double contamination);

/**
 * Give a color code corresponding to the building
 * @param type the building type
 * @return a color code
 */
char* get_color_prefix_from_building_type(Building_type type);