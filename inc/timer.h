/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file timer.h
 */

#ifndef __TIMER_H
#define __TIMER_H

#define EVERYONE 0
#define DELAY 1

/**
 * Setup then start the timer, and ticks every 1 second by default.
 */
void setup_timer();

/**
 * Send a SIGUSR1 signal to every process by default.
 * @param sig the signal
 */
void tick_new_turn(int sig);

#endif

