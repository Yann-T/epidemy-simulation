/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file tiles.h
 */

#ifndef __TILES_H
#define __TILES_H

#include "citizen/citizen_base_functions.h"

/**
 * Play the turns of each tiles when receiving a notification
 * @param args the argument of the thread
 */
void* handle_tiles_thread(void* args);

/**
 * Contaminates the nearby tiles
 * @param city the city
 * @param tile the tile who is going to contaminate the other tiles
 */
void contaminate_other_tiles(City * city, Tile tile);

#endif