/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file normal_citizen.h
 */

#ifndef __NORMAL_CITIZEN_H
#define __NORMAL_CITIZEN_H

#include "thread_manager.h"
#include "citizen/citizen_base_functions.h"

/**
 * Creates the thread for a normal citizen
 * @param status A pointer to the status of this citizen in the shared memory
 * @param x the x position of the citizen in the city
 * @param y the y position of the citizen in the city
 * @param city a pointer to the city in the shared memory
 * @return The pthread_t of the started thread.
 */
pthread_t create_normal_citizen(Citizen_data* data);

/**
 * Handle the behaviour of a normal citizen
 * @param args The data of the citizen
 */
void* handle_normal_citizen(void* args);

/**
 * Make a citizen play his turn
 * @parma data the data of the citizen
 */
void play_turn_normal(Citizen_data * data);


#endif
