/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file citizen_base_functions.h
 */

#ifndef __CITIZEN_BASE_FUNCTION_H
#define __CITIZEN_BASE_FUNCTION_H

#include "city.h"
#include "thread_manager.h"

/**
 * Determinate if the citizen will move and return if he did
 * @param data the data of the citizen
 * @return 0 if he didn't move, 1 if he did.
 */
int move_normal(Citizen_data * data);

/**
 * Make a citizen move via its Status.
 * @param citizen the citizen to move
 * @param city the city in which the citizen is moving
 * @param x the x coordinate of the new position
 * @param y the y coordinate of the new position
 */
void move_citizen(Status * citizen, City * city, int x, int y);

/**
 * Add contamination to a citizen
 * @param citizen the citizen to contaminate
 * @param contamination_amount how much contamination to add
 */
void contaminate_citizen(Status * citizen, double contamination_amount);

/**
 * Remove contamination from a citizen
 * @param citizen the citizen to decontaminate
 * @param contamination_amount how much contamination to remove
 */
void decontaminate_citizen(Status * citizen, double contamination_amount);

/**
 * Add contamination to a tile
 * @param tile the tile to contaminate
 * @param contamination_amount how much contamination to add
 */
void contaminate_tile(Tile * tile, double contamination_amount);

/**
 * Remove contamination from a tile
 * @param tile the tile to decontaminate
 * @param contamination_amount how much contamination to remove
 */
void decontaminate_tile(Tile * tile, double contamination_amount);

/**
 * Have a chance to make the citizen sick
 * @param data the status of the citizen
 */
void process_sickness(Status * citizen);

/**
 * Add contamination to the nearby citizens
 * @param data the data of the citizen
 */
void other_citizen_contamination(Citizen_data * data);

/**
 * Kill the given citizen
 * @param citizen the citizen to kill
 */
void kill_citizen(Status * citizen);

/**
 * Add contamination caused by the tile
 * @param data the data of the citizen
 * @patam has_move represent if the citizen has moved this turn
 */
void tile_contamination(Citizen_data * data, int has_moved);

/**
 * Checks if a tile has a firefighter in it
 * @param citizens The array with all citizens
 * @param x The x coordinate of the tile
 * @param y The y coordinate of the tile
 * @return 1 if a tile has a firefighter in it
 */
int check_if_tile_has_firefighter(Status * citizens, unsigned int x, unsigned int y);

/**
 * Checks if a citizen is allowed in a tile
 * @param data The data of the citizen
 * @param x The x coordinate of the tile
 * @param y The y coordinate of the tile
 * @return 1 if the citizen is allowed in the tile
 */
int is_allowed_in(Citizen_data * data, unsigned int x, unsigned int y);

/**
 * Computes the chebyshev distance between two points
 * @param the x coordinate of the first point
 * @param the y coordinate of the first point
 * @param the x coordinate of the second point
 * @param the y coordinate of the second point
 * @return the chebyshev distance
 */
int chebyshev_distance(int x1, int y1, int x2, int y2);

/**
 * Get the max value between two integers
 * @param a
 * @param b
 * @return the max value between a and b
 */ 
int max ( int a, int b );

/**
 * Get all the status of the citizens in a tile
 * @param x the x coordinate of the tile
 * @param y the y coordinate of the tile
 * @param city a pointer to the city
 * @param size A pointer to the size of the array
 * @return an array with all the status of the citizens in a tile
 */
Status ** get_citizens_in_tile(unsigned int x, unsigned int y, City * city, int * size);

#endif
