/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file doctor.h
 */

#ifndef __DOCTOR_H
#define __DOCTOR_H

#include "thread_manager.h"
#include "citizen/citizen_base_functions.h"

/**
 * Creates the thread for a doctor
 * @param data The data of the citizen
 * @return The pthread_t of the started thread.
 */
pthread_t create_doctor(Citizen_data* data);

/**
 * Handle the behaviour of a doctor
 * @param args The data of a citizen
 */
void * handle_doctor(void* args);

/**
 * Make a doctor play his turn
 * @param data The data of the citizen
 * @param nb_blood_bag The number of blood bag the doctor has
 * @param days_spent_outside_hospital The number of days spent outside the hospital by the doctor
 */
void play_turn_doctor(Citizen_data * data, int * nb_blood_bag, int * days_spent_outside_hospital);

/**
 * Heal a person if possible
 * @param data The data of the citizen
 * @param nb_blood_bag The number of blood bag the doctor has
 * @param days_spent_outside_hospital The number of days spent outside the hospital by the doctor
 */
void process_healing(Citizen_data * data, int * nb_blood_bag, int * days_spent_outside_hospital);

/**
 * Heals a citizen
 * @param status The citizen to heal
 */
void heal(Status * status);

/**
 * Finds the most contaminated citizen in a tile
 * @param city The city
 * @param x The x coordinate of the tile
 * @param y The y coordinate of the tile
 * @return The sickest citizen in the tile or Null if there is no one
 */
Status * find_sickest_citizen(City * city, unsigned int x, unsigned int y);

/**
 * Makes the doctor move on the map
 * @param data The data of the citizen
 * @param days_spent_outside_hospital The number of days spent outside the hospital by the doctor
 * @return 1 if the doctor has moved
 */
int move_doctor(Citizen_data * data, int days_spent_outside_hospital);

#endif
