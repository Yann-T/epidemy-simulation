/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file citizen_base_functions.h
 */

#ifndef __FIREFIGHTER_H
#define __FIREFIGHTER_H

 #include <stdlib.h>

#include "thread_manager.h"
#include "citizen/citizen_base_functions.h"

/**
 * Creates the thread for a firefighter
 * @param data The data of the citizen
 * @return The pthread_t of the started thread.
 */
pthread_t create_firefighter(Citizen_data* data);

/**
 * Handle the behaviour of a firefighter
 * @param args The data of a citizen
 */
void* handle_firefighter(void* args);

/**
 * Make a firefighter play his turn
 * @param data the data of the firefighter
 * @param pulverisator_capacity the capacity in the fire
 */
void play_turn_firefighter(Citizen_data * data, double * pulverisator_capacity);

/**
 * Burn the corpses on a tile
 * @param x the x coordinate of the tile
 * @param y the y coordinate of the tile
 * @param city the city
 */
void burn_corpses(unsigned int x, unsigned int y, City * city);

/**
 * Decontaminate a tile and the citizens within it
 * @param x the x coordinate of the tile
 * @param y the y coordinate of the tile
 * @param data the data of the firefighter
 * @param pulverisator_capacity the capacity of the firefighter's pulverisator
 */
void decontaminate_citizens_and_tile(int x, int y, Citizen_data * data, double * pulverisator_capacity);

/**
 * @param x the x coordinate of the tile
 * @param y the y coordinate of the tile
 * @param data the data of the firefighter
 * @param pulverisator_capacity the capacity of the firefighter's pulverisator
 */
void refill_pulverisator(int x, int y, Citizen_data * data, double * pulverisator_capacity);

#endif