/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file reporter.h
 */

#ifndef __REPORTER_H
#define __REPORTER_H

#include <mqueue.h>

#include "thread_manager.h"

#define URGENT 10
#define IMPORTANT 5
#define INFORMATIVE 2
#define NO_ONE_CARES 1

/**
 * Creates the thread for a reporter
 * @param data The data of the citizen
 * @return The pthread_t of the started thread.
 */
pthread_t create_reporter(Citizen_data* data);

/**
 * Defines the behavior of a reporter once created.
 * @param args Previously allocatd data of the reporter.
 **/
void * handle_reporter(void* args);

/**
 * Makes a reporter play his turn
 * @param  data the data of the citizen
 */
void play_turn_reporter(Citizen_data * data, mqd_t mqueue);

/**
 * Send the reports of the reporter
 * @param data the data of the citizen
 * @param mqueue the message queue descriptor
 */
void send_reports(Citizen_data* data, mqd_t mqueue);

#endif
