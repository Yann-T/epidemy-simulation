/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file epidemy_const.h
 */

#ifndef __EPIDEMY_CONST_H
#define __EPIDEMY_CONST_H

/* Paths */
#define SHARED_MEM "/shmcity"
#define MQUEUE_NAME "/journalists_to_agency"
#define TIMER_PATH "bin/timer"
#define CITIZEN_MANAGER_PATH "bin/citizen_manager"
#define GNUPLOT_EVOLUTION_PATH "misc/gnuplot/evolution.txt"
#define CITIZEN_NAMES_FILE_PATH "misc/names.txt"

/* Simulation rule */
#define NUMBER_OF_TURNS 100

/* Other properties */
#define CITIZEN_NAME_MAX_LENGHT 50
#define NCURSES_DISPLAY 1

/* Map properties */
#define CITY_HEIGHT 7
#define CITY_WIDTH 7
#define NB_CITIZENS 25
#define NB_FIREFIGHTERS 6
#define NB_DOCTORS 4
#define NB_REPORTERS 2
#define NB_TOTAL NB_CITIZENS + NB_REPORTERS + NB_DOCTORS + NB_FIREFIGHTERS
#define NB_HOUSES CITY_HEIGHT*CITY_WIDTH/4
#define HOUSE_MAX_CAPACITY 6
#define FIRESTATION_MAX_CAPACITY 8
#define HOSPITAL_MAX_CAPACITY 12
#define WASTELAND_MAX_CAPACITY 16

/* Defines the contamination level of the wasteland
 * at the beginning as percentages
 */
#define MIN_STARTING_CONTAMINATION 0.2
#define MAX_STARTING_CONTAMINATION 0.4
#define WASTELAND_CONTAMINATION_PROBABILITY 0.1

/* Default values */
#define DEFAULT_CITIZEN_CONTAMINATION 0
#define DEFAULT_CITIZEN_SICKNESS_DURATION 0
#define DEFAULT_CITIZEN_IS_SICK 0
#define DEFAULT_TILE_CONTAMINATION_LEVEL 0

/* Normal citizen game rules */
#define NORMAL_CITIZEN_CHANCE_TO_MOVE 0.4
#define HAS_MOVED_GET_CONTAMINATION_FROM_TILE 0.02
#define DID_NOT_MOVE_GET_CONTAMINATION_FROM_TILE 0.05

/* Doctor game rules */
#define NB_BLOOD_BAG_AT_THE_START 5
#define NB_BLOOD_BAG_IN_HOSPITAL 10
#define DAYS_OUTSIDE_UNTIL_GOING_IN_THE_HOSPITAL 3

/* Firefighter game rules */
#define INITIAL_PULVERISATOR_CAPACITY 10
#define INITIAL_PULVERISATOR_CAPACITY_REDUCTION_WHEN_OUTSIDE_FIRESTATION 2
#define PULVERISATOR_USED_EACH_TURN 0.1;
#define FIREFIGHTER_TILE_CONTAMINATION_RESISTANCE 0.1
#define FIREFIGHTER_CHANCE_TO_RESIST_CONTAMINATION_FROM_OTHERS 0.7
#define MAX_CAPACITY_FOR_A_SINGLE_CITIZEN 0.2

/* All citizen type game rules */
#define TILES_CONTAMINATION_RATE 0.01
#define DAYS_CONTAMINATED_BEFORE_RISK_OF_DEATH 5
#define CHANCE_TO_DIE_OF_SICKNESS_PER_DAY 0.05
#define CHANCE_TO_CONTAMINATE_OTHER_ON_SAME_TILE 0.1
#define CHANCE_TO_CONTAMINATE_OTHER_ON_NEIGHBOURG_TILE 0.01
#define DAYS_ALLOWED_IN_HOSPITAL 2
#define AMOUNT_OF_CONTAMINATION_TRANSMITED 0.1

/* Tiles game rules */
#define HOSPITAL_CONTAMINATION_RATE 0.25
#define WASTELAND_CHANCE_TO_CONTAMINATE_OTHER_TILES 0.15
#define WASTELAND_MAX_PERCENTAGE_OF_DIFF_CONTAMINATE 0.2

/* Bonus rules (useful when the map is bigger) */
#define ENABLE_BONUS_RULES 0
#define CHANCE_TO_CREATE_ADDITIONAL_HOSPITAL 0.015
#define CHANCE_TO_CREATE_ADDITIONAL_FIRESTATION 0.015

#endif
