/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file epidemic_sim.c
 */

#include <stdio.h>
#include <mqueue.h>
#include <time.h>

#include "epidemic_sim_exec.h"
#include "epidemic_evolution.h"
#include "city.h"
#include "epidemy_const.h"

#if NCURSES_DISPLAY
#include "display/ncurses_display.h"
#else
#include "display/basic_display.h"
#endif

/* Global scope is necessary in order to be handled by a signal handler */
City* c;
FILE * evolution;
int turn;

int create_shared_memory() {
	int mem;
	mem = shm_open(SHARED_MEM, O_CREAT | O_RDWR, S_IRWXU);

	if(mem < 0) {
		perror("shm_open()");
		exit(-1);
	}

	if(ftruncate(mem, sizeof(City)) < 0) {
		perror("ftruncate()");
		exit(-1);
	}

	return mem;
}

mqd_t create_mqueue() {
	mqd_t mqueue;
	mqueue = mq_open(MQUEUE_NAME, O_CREAT | O_RDWR,
					 S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH, NULL);

	if(mqueue < 0) {
		perror("mq_open()");
		exit(EXIT_FAILURE);
	}
	return mqueue;
}

void process_new_turn(int sig) {
	sig = sig;
	if(++turn <= NUMBER_OF_TURNS) {
		Statistics stats = get_epidemic_evolution(c);
		#if NCURSES_DISPLAY
		draw_screen_ncurses(turn, stats, *c, 0, 0);
		#else
		draw_screen(turn, stats, *c);
		#endif
		write_statistics(evolution, stats, turn);
	}
}

int main() {
	int mem;
	mqd_t mqueue;
	
	srand(time(NULL));
	mem = create_shared_memory();
	mqueue = create_mqueue();

	c = mmap(NULL, sizeof(City), PROT_READ | PROT_WRITE, MAP_SHARED, mem, 0);
	init_city(c);
	evolution = fopen(GNUPLOT_EVOLUTION_PATH, "w");
	fprintf(evolution, "%d,%d,%d,%d,%d\n", 0,NB_TOTAL,0,0,0);
	setup_on_signal(SIGUSR1, process_new_turn);
	setup_on_signal(SIGINT, end_game);
	start_game();

	getchar();

	end_game();
	fclose(evolution);
	if(munmap(c, sizeof(City)) < 0) {
		perror("munmap()");
	}
	if(close(mem) < 0) {
		perror("close(mem)");
	}
	if(shm_unlink(SHARED_MEM) < 0) {
		perror("shm_unlink()");
	}
	if(mq_close(mqueue) < 0) {
		perror("mq_close()");
	}
	if(mq_unlink(MQUEUE_NAME) < 0) {
		perror("mq_unlink()");
	}

	exit(EXIT_SUCCESS);
}
