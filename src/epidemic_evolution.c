/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file epidemic_evolution.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "epidemic_evolution.h"
#include "city.h"

Statistics init_stats() {
	Statistics stats;
	stats.healthy = 0;
	stats.sick = 0;
	stats.dead = 0;
	stats.burned = 0;
	return stats;
}

Statistics get_epidemic_evolution(City* city) {
	unsigned int i;
	Status status;
	Statistics city_stats;
	city_stats = init_stats();
	for (i = 0; i < NB_TOTAL; ++i) {
		status = city->citizens[i];
		city_stats.sick += (status.is_sick == 1);
		city_stats.dead += ((status.type == DEAD) || (status.type == BURNED));
		city_stats.burned += (status.type == BURNED);
	}
	city_stats.healthy = NB_TOTAL - city_stats.sick - city_stats.dead;
	return city_stats;
}

void write_statistics(FILE * evolution, Statistics stats, int turn) {
	fprintf(evolution, "%d,%d,%d,%d,%d\n", turn, stats.healthy, stats.sick,stats.dead, stats.burned);
	fflush(evolution);
}
