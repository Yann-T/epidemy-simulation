/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file city.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "city.h"

void init_building(City * city, int x, int y, Building_type building,
										int capacity, double contamination) {
	city->map[x][y].type = building;
	city->map[x][y].x = x;
	city->map[x][y].y = y;
	city->map[x][y].capacity = capacity;
	city->map[x][y].contamination = contamination;
	city->map[x][y].nb_citizens = 0;
}

void init_city(City* city) {
	unsigned int i,j;
	unsigned int x, y;
	double contamination;
	/* Initialization of the map as full of wasteland*/
	for (i = 0; i < CITY_HEIGHT; ++i) {
		for (j = 0; j < CITY_WIDTH; ++j) {
			contamination = 0;
			if (rand()%100 < WASTELAND_CONTAMINATION_PROBABILITY*100) {
				contamination = ((rand() % (int)(MAX_STARTING_CONTAMINATION * 100 - MIN_STARTING_CONTAMINATION * 100) + MIN_STARTING_CONTAMINATION * 100)/100.0);
			}
			
			init_building(city, j, i, WASTELAND, WASTELAND_MAX_CAPACITY, contamination);
		}
	}
	/*Initialization of the firestations and hospital*/
	init_building(city, 0, CITY_HEIGHT-1, FIRESTATION, FIRESTATION_MAX_CAPACITY, DEFAULT_TILE_CONTAMINATION_LEVEL);
	init_building(city, CITY_WIDTH-1, 0, FIRESTATION, FIRESTATION_MAX_CAPACITY, DEFAULT_TILE_CONTAMINATION_LEVEL);
	init_building(city, (CITY_WIDTH-1)/2, (CITY_HEIGHT-1)/2, HOSPITAL, HOSPITAL_MAX_CAPACITY, DEFAULT_TILE_CONTAMINATION_LEVEL);

	/*Initialization of the houses*/
	for (i = 0; i < NB_HOUSES; i++) {
		do {
			x = rand()%CITY_WIDTH;
			y = rand()%CITY_HEIGHT;
		} while(city->map[x][y].type != WASTELAND);
		init_building(city, x, y, HOUSE, HOUSE_MAX_CAPACITY, DEFAULT_TILE_CONTAMINATION_LEVEL);
	}
	#if ENABLE_BONUS_RULES
	init_bonus_buildings(city);
	#endif
}

void init_bonus_buildings(City* city) {
	unsigned int i,j;

	for (i = 0; i < CITY_HEIGHT; ++i) {
		for (j = 0; j < CITY_WIDTH; ++j) {
			if(city->map[j][i].type == WASTELAND || city->map[j][i].type == HOUSE) {
				if((rand() % 100) < (CHANCE_TO_CREATE_ADDITIONAL_HOSPITAL * 100)) {
					init_building(city, j, i, HOSPITAL, HOSPITAL_MAX_CAPACITY, 0);			
				}
				if((rand() % 100) < (CHANCE_TO_CREATE_ADDITIONAL_FIRESTATION * 100)) {
					init_building(city, j, i, FIRESTATION, FIRESTATION_MAX_CAPACITY, 0);			
				}
			}
		}
	}
}

int get_number_of_living_citizen_on_tile(City city, Tile tile) {
	int i, n;
	Status citizen;

	n = 0;
	for (i = 0; i < NB_TOTAL; ++i) {
		citizen = city.citizens[i];
		if( (citizen.x == tile.x) &&
			(citizen.y == tile.y) &&
			(citizen.type != DEAD) &&
			(citizen.type != BURNED)) {
			n++;
		}
	}
	return n;
}