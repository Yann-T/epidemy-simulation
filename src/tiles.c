/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file tiles.c
 */

#include "tiles.h"

extern pthread_cond_t cond;
extern pthread_mutex_t mutex;

void* handle_tiles_thread(void* args) {
	unsigned int i,j;
	City * city;
	
	city = (City*)args;
	
	while(1) {
		for (i = 0; i < CITY_HEIGHT; ++i) {
			for (j = 0; j < CITY_WIDTH; ++j) {
				contaminate_other_tiles(city, city->map[j][i]);
			}
		}
		pthread_cond_wait(&cond, &mutex);
	}
	return NULL;
}

void contaminate_other_tiles(City * city, Tile tile) {
	int i, j;
	double diff;
	int contaminate;
	double contamination_value;
	Tile* target;

	for (i = -1; i <= 1; ++i) {
		for (j = -1; j <= 1; ++j) {

			if( (((int)tile.x + j) >= 0) &&
				((tile.x + j) < CITY_WIDTH) &&
				(((int)tile.y + i) >= 0) &&
				((tile.y + i) < CITY_HEIGHT)) {

				target = &city->map[tile.x + j][tile.y + i];

				if(target->type == WASTELAND) {

					contaminate = (rand()%100) < (WASTELAND_CHANCE_TO_CONTAMINATE_OTHER_TILES * 100);

					diff = tile.contamination - target->contamination;
					if((contaminate) && (diff > 0)) {
						contamination_value = ((rand()% (int)(WASTELAND_MAX_PERCENTAGE_OF_DIFF_CONTAMINATE * 100)) + 1)/100.0 * diff;
						contaminate_tile(target, contamination_value);
					}
				}
			}
		}
	}
}