/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file thread_manager.c
 */

#include <string.h>

#include "epidemy_const.h"
#include "citizen/reporter.h"
#include "citizen/firefighter.h"
#include "citizen/normal_citizen.h"
#include "tiles.h"
#include "thread_manager.h"
#include "name_picker.h"
#include "citizen/doctor.h"

pthread_cond_t cond;
pthread_mutex_t mutex;

void handle_signal() {
	pthread_cond_broadcast(&cond);
}

void* handle_notifier() {
	struct sigaction action;

	action.sa_handler = &handle_signal;
	sigaction(SIGUSR1, &action, NULL);
	return NULL;
}

pthread_t create_notifier_thread() {
	pthread_t id;

	if(pthread_create(&id, NULL, handle_notifier, NULL) < 0) {
		perror("pthread_create()");
	}
	return id;
}

pthread_t create_tile_manager(City * city) {
	pthread_t id;
	if(pthread_create(&id, NULL, handle_tiles_thread, (void*)city) < 0) {
		perror("pthread_create()");
	}
	return id;
}

/* Population creation */
pthread_t create_citizen(Status * status, Citizen_type type, int x, int y, City * city, int number_names, FILE* file) {
	Citizen_data* data;
	
	data = malloc(sizeof(Citizen_data));
	data->status = status;
	data->city = city;

	init_citizen(data, type, x, y, number_names, file);
	city->map[x][y].nb_citizens++;
	switch(type) {
		case NORMAL: return create_normal_citizen(data);
		case REPORTER: return create_reporter(data);
		case DOCTOR: return create_doctor(data);
		case FIREFIGHTER: return create_firefighter(data);
		default : return 0;
	}
}

void init_citizen(Citizen_data * data, Citizen_type type, int x, int y, int number_names, FILE * file) {
	char* name;

	data->status->x = x;
	data->status->y = y;
	name = get_random_name_from_file(file, number_names);
	strcpy(data->status->name, name);
	free(name);
	data->status->is_sick = DEFAULT_CITIZEN_IS_SICK;
	data->status->contamination = DEFAULT_CITIZEN_CONTAMINATION;
	data->status->sickness_duration = DEFAULT_CITIZEN_SICKNESS_DURATION;
	data->status->type = type;
	data->status->days_spent_in_hospital_healthy = 0;
}

pthread_t* create_population(City* city) {
	pthread_t t_id[NB_TOTAL];
	FILE* file;
	int number_names = 0;

	file = fopen(CITIZEN_NAMES_FILE_PATH, "r");
	create_notifier_thread();
	number_names = get_number_of_line_from_file(file);
	
	create_tile_manager(city);

	for (int i = 0; i < NB_CITIZENS; ++i) {
		t_id[i] = create_citizen(&(city->citizens[i]), NORMAL, (rand()%CITY_WIDTH), (rand()%CITY_HEIGHT), city, number_names, file);
	}

	for (int i = NB_CITIZENS; i < NB_CITIZENS + NB_REPORTERS; i++) {
		t_id[i] = create_citizen(&(city->citizens[i]), REPORTER, (rand()%CITY_WIDTH), (rand()%CITY_HEIGHT), city, number_names, file);
	}	

	if(NB_DOCTORS > 0) {
		t_id[NB_CITIZENS + NB_REPORTERS] = create_citizen(&(city->citizens[NB_CITIZENS + NB_REPORTERS]), DOCTOR, (CITY_WIDTH-1)/2, (CITY_HEIGHT-1)/2, city, number_names, file);
	}

	for (int i = NB_CITIZENS + NB_REPORTERS + 1; i < NB_CITIZENS + NB_REPORTERS + NB_DOCTORS; i++) {
		t_id[i] = create_citizen(&(city->citizens[i]), DOCTOR, (rand()%CITY_WIDTH), (rand()%CITY_HEIGHT), city, number_names, file);
	}

	/* Initializing firefighters on the two firestations */
	if(NB_FIREFIGHTERS > 0) {
		t_id[NB_CITIZENS + NB_REPORTERS + NB_DOCTORS] = create_citizen(&(city->citizens[NB_CITIZENS + NB_REPORTERS + NB_DOCTORS]), FIREFIGHTER,  0, CITY_HEIGHT-1, city, number_names, file);
	}
	if(NB_FIREFIGHTERS > 1) {
		t_id[NB_CITIZENS + NB_REPORTERS + NB_DOCTORS + 1] = create_citizen(&(city->citizens[NB_CITIZENS + NB_REPORTERS + NB_DOCTORS + 1]), FIREFIGHTER,  CITY_WIDTH-1, 0, city, number_names, file);
	}

	for (int i = NB_CITIZENS + NB_REPORTERS + NB_DOCTORS + 2; i < NB_CITIZENS + NB_REPORTERS + NB_DOCTORS + NB_FIREFIGHTERS; i++) {
		t_id[i] = create_citizen(&(city->citizens[i]), FIREFIGHTER, (rand()%CITY_WIDTH), (rand()%CITY_HEIGHT), city, number_names, file);
	}

	fclose(file);

	for (int i = 0; i < NB_TOTAL; ++i) {
		pthread_join(t_id[i], NULL);
	}
	
	return NULL;
}

