/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file citizen_manager.c
 */

#include "thread_manager.h"
#include "epidemy_const.h"

int main() {
	City *city;
	int shmd;

	/*Opening shared memory*/
	shmd = shm_open(SHARED_MEM, O_RDWR, S_IRUSR | S_IWUSR);
	if (shmd == -1) {
		perror("shm_open()");
		exit(-1);
	}

	city = mmap(NULL, sizeof(City), PROT_READ | PROT_WRITE,
				 MAP_SHARED, shmd, 0);

	create_population(city);

	return 0;
}