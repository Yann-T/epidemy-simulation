/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file press_agency.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <mqueue.h>

#include "epidemy_const.h"
#include "citizen/reporter.h"
#include "press_agency.h"


mqd_t open_mqueue() {
	return mq_open(MQUEUE_NAME, O_RDONLY);
}

void handle_dead_citizens_report(char * message) {
	unsigned int dead_citizens;
	char name[CITIZEN_NAME_MAX_LENGHT];
	sscanf(message, "%u %[^\t\n]", &dead_citizens, name);
	dead_citizens = (1-0.35)*dead_citizens;
	if (dead_citizens) {
		printf("NEWS:%d dead citizens, reports our special envoy, %s\n", dead_citizens, name);
	}
}

void handle_city_contamination_level_report(char * message) {
	double contamination_level;
	char name[CITIZEN_NAME_MAX_LENGHT];
	sscanf(message, "%lf %[^\t\n]", &contamination_level, name);
	contamination_level = (1-0.1)*contamination_level;
	printf("NEWS: %2.2f%% contamination estimated by specialist %s\n", contamination_level*100, name);
}

void handle_contaminated_citizens_report(char * message) {
	unsigned int contaminated_citizens;
	char name[CITIZEN_NAME_MAX_LENGHT];
	sscanf(message, "%u %[^\t\n]", &contaminated_citizens, name);
	contaminated_citizens = (1-0.1)*contaminated_citizens;
	if (contaminated_citizens) {
		printf("NEWS: %d contaminated citizens, says %s\n", contaminated_citizens, name);
	}
}

void handle_reporter_contamination_level_report(char * message) {
	double contamination_level;
	char name[CITIZEN_NAME_MAX_LENGHT];
	sscanf(message, "%lf %[^\t\n]", &contamination_level, name);
	if (contamination_level > 0.8) {
		printf("NEWS: %lf BUT STILL ALIVE, says our journalist, %s\n", contamination_level*100, name);
	}
}

void handle_message(char * message, unsigned int priority) {
	switch(priority) {
	case URGENT:
		handle_dead_citizens_report(message);
		break;
	case IMPORTANT:
		handle_city_contamination_level_report(message);
		break;
	case INFORMATIVE:
		handle_contaminated_citizens_report(message);
		break;
	case NO_ONE_CARES:
		handle_reporter_contamination_level_report(message);
		break;
	default:
		printf("Unhandled message received: %s\n", message);
	}
}

int main() {
	unsigned int priority;
	mqd_t mqueue;
	struct mq_attr attr;
	char *buffer = NULL;
	mqueue = open_mqueue();
	mq_getattr(mqueue, &attr);
	buffer = malloc(attr.mq_msgsize);
	while(1) {
		mq_receive(mqueue, buffer, attr.mq_msgsize, &priority);
		handle_message(buffer, priority);
	}
	mq_close(mqueue);
	exit(EXIT_SUCCESS);
}
