/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file epidemic_sim_exec.c
 */

#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

#include "epidemy_const.h"
#include "epidemic_sim_exec.h"

#if NCURSES_DISPLAY
#include "display/ncurses_display.h"
#endif

/* Global scope is necessary in order to be handled by a signal handler */
pid_t pid_timer = 0;
pid_t pid_citizen_manager = 0;
pid_t pid_gnuplot = 0;

pid_t start_exec(const char * path, const char * args) {
	pid_t pid;
	pid = fork();
	if (pid < 0) {
		perror("fork()");
		exit(EXIT_FAILURE);
	} else if (pid == 0) {
		printf("%s launched, pid: %d, grp: %d\n", path, getpid(), getpgrp());
		execl(path, args, (void*)NULL);
		exit(EXIT_SUCCESS);
	}
	return pid;
}

void setup_on_signal(int sig, void (*process)(int)) {
	signal(sig, process);
}

void start_timer() {
	pid_timer = start_exec(TIMER_PATH, "");
}

void start_citizen_manager() {
	pid_citizen_manager = start_exec(CITIZEN_MANAGER_PATH, "");
}

void start_gnuplot() {
	pid_t pid;
	pid = fork();
	if (pid < 0) {
		perror("fork()");
		exit(EXIT_FAILURE);
	} else if (pid == 0) {
		setpgid(pid, 0);
		/*We need to change gnuplot's group id, or else any SIGUSR1 will kill the
		 *process. Unfortunately, we can't deal with how gnuplot handles it*/
		printf("gnuplot launched, pid: %d, grp: %d\n", getpid(), getpgrp());
		execlp("gnuplot", "gnuplot", "misc/gnuplot/evolution.gp", "-noraise",  (void*)NULL);
		exit(EXIT_SUCCESS);
	}
	pid_gnuplot = pid;
}

void start_game() {
	#if NCURSES_DISPLAY
	initscr();
	init_ncurses_colors();
	#endif
	start_timer();
	start_citizen_manager();
	sleep(2);
	start_gnuplot();
}

void end_game() {
	#if NCURSES_DISPLAY
	endwin();
	#endif
	printf("Terminating child process…\n");
	kill(pid_timer, SIGKILL);
	kill(pid_citizen_manager, SIGKILL);
	kill(pid_gnuplot, SIGKILL);
	exit(EXIT_FAILURE);
}