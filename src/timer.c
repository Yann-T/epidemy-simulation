/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file timer.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

#include "timer.h"

void do_nothing() {}

void loop_timer(struct sigaction * action) {
    action->sa_handler = &tick_new_turn;
    sigaction(SIGALRM, action, NULL);
}

void usr1_timer(struct sigaction * action) {
    action->sa_handler = &do_nothing;
    sigaction(SIGUSR1, action, NULL);
}

void setup_timer() {
    struct sigaction action;
    struct sigaction action_dummy;
    memset(&action, 0, sizeof(action));
    memset(&action_dummy, 0, sizeof(action_dummy));
    loop_timer(&action);
    usr1_timer(&action_dummy);
}

void tick_new_turn(int sig) {
    sig = sig;
    kill(EVERYONE, SIGUSR1);
    alarm(DELAY);
}

int main() {
    setup_timer();
    alarm(DELAY);
    while(1){}
    exit(EXIT_SUCCESS);
}
