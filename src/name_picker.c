/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file name_picker.c
 */

#include "name_picker.h"

char* get_random_name_from_file(FILE* file, int nb_lines) {
	int line_to_pick, count;
	char* line;
	
	line = (char*)malloc(sizeof(char) * CITIZEN_NAME_MAX_LENGHT);
	line_to_pick = rand() % nb_lines;
	count = 0;
	fseek(file, 0, SEEK_SET);

	while (fgets(line, CITIZEN_NAME_MAX_LENGHT, file)) {
		if (count == line_to_pick) {
			strtok(line, "\n");
			return line;
		}
		count++;
	}

	return "Unamed";
}

int get_number_of_line_from_file(FILE* file) {
	int n;
	char chr;
	
	chr = getc(file);
	n = 0;

	while (chr != EOF) {
		if (chr == '\n') {
			n++;
		}
		chr = getc(file);
	}
	
	return n;
}