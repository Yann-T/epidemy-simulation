/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file citizen_base_functions.c
 */

#include "citizen/citizen_base_functions.h"

/* Functions common to all the citizens */

int move_normal(Citizen_data * data) {
	unsigned int next_x, next_y, do_move;

	do_move = (rand()%100) < (NORMAL_CITIZEN_CHANCE_TO_MOVE * 100);


	if(do_move || !is_allowed_in(data, data->status->x, data->status->y)) {
		do {
			next_x = data->status->x + (rand()%3) - 1;
			next_y = data->status->y + (rand()%3) - 1;
		} while(((next_x == data->status->x) && (next_y == data->status->y))
			|| !is_allowed_in(data, next_x, next_y));

		move_citizen(data->status, data->city, next_x, next_y);
		return 1;
	}
	return 0;
}


void move_citizen(Status * citizen, City * city, int x, int y) {
	int old_x;
	int old_y;
	
	old_x = citizen->x;
	old_y = citizen->y;

	citizen->x = x;
	citizen->y = y;

	city->map[old_x][old_y].nb_citizens--;
	city->map[x][y].nb_citizens++;
}

void contaminate_citizen(Status * citizen, double contamination_amount) {
	/* Handle special cases */
	if(citizen->type == DEAD || citizen->type == BURNED) {
		return;
	}
	citizen->contamination += contamination_amount;
	citizen->contamination = (citizen->contamination > 1) ? 1 : citizen->contamination;
}

void decontaminate_citizen(Status * citizen, double contamination_amount) {
	if(citizen->type == DEAD || citizen->type == BURNED) {
		return;
	}
	citizen->contamination -= contamination_amount;
	citizen->contamination = (citizen->contamination < 0) ? 0 : citizen->contamination;
}

void contaminate_tile(Tile * tile, double contamination_amount) {
	/* Firestations always have a contamination level of zero */
	if(tile->type == FIRESTATION) {
		return;
	}
	/* Hospitals reduces the gained contamination level */
	if(tile->type == HOSPITAL) {
		contamination_amount *= HOSPITAL_CONTAMINATION_RATE;
	}
	
	tile->contamination += contamination_amount;
	tile->contamination = (tile->contamination > 1) ? 1 : tile->contamination;
}

void decontaminate_tile(Tile * tile, double contamination_amount) {
	tile->contamination -= contamination_amount;
	tile->contamination = (tile->contamination < 0) ? 0 : tile->contamination;
}

void process_sickness(Status * citizen) {
	int become_sick, die;

	if(citizen->type == DEAD || citizen->type == BURNED) {
		return;
	}

	if(citizen->is_sick == 0) {
		become_sick = (rand()%100) < (citizen->contamination * 100);

		if(become_sick) {
			citizen->is_sick = 1;
		}
	} else {
		citizen->sickness_duration++;
	}
	if(citizen->sickness_duration > DAYS_CONTAMINATED_BEFORE_RISK_OF_DEATH) {
		die = (rand()%100) < ((citizen->sickness_duration - DAYS_CONTAMINATED_BEFORE_RISK_OF_DEATH) * CHANCE_TO_DIE_OF_SICKNESS_PER_DAY * 100);
		if(die) {
			kill_citizen(citizen);
		}
	}
}

void kill_citizen(Status * citizen) {
	citizen->type = DEAD;
	citizen->is_sick = 0;
}

void other_citizen_contamination(Citizen_data * data) {
	unsigned int citizen_x, citizen_y;
	int i, apply_contamination;
	Status * citizens;

	citizens = data->city->citizens;
	citizen_x = data->status->x;
	citizen_y = data->status->y;

	for (i = 0; i < NB_TOTAL; i++) {

		if(&citizens[i] != data->status) {
		/* Check if the citizen is exactly on the same tile */
			if( (citizens[i].x == citizen_x) &&
				(citizens[i].y == citizen_y) ) {
				
				apply_contamination = (rand()%100) < (CHANCE_TO_CONTAMINATE_OTHER_ON_SAME_TILE * 100);
				if(apply_contamination) {
					if(citizens[i].type == FIREFIGHTER) {
						if((rand()%100) < (FIREFIGHTER_CHANCE_TO_RESIST_CONTAMINATION_FROM_OTHERS * 100)) {
							contaminate_citizen(&citizens[i], AMOUNT_OF_CONTAMINATION_TRANSMITED * data->status->contamination);
						}
					}else {
						contaminate_citizen(&citizens[i], AMOUNT_OF_CONTAMINATION_TRANSMITED * data->status->contamination);
					}
				}
			}
			/* If the tile is a wasteland, also check the neighbours wastelands */
			if( (data->city->map[citizen_x][citizen_y].type == WASTELAND) &&
				(chebyshev_distance(citizens[i].x, citizens[i].y, citizen_x, citizen_y) == 1) &&
				(data->city->map[citizens[i].x][citizens[i].y].type == WASTELAND) ) {

				apply_contamination = (rand()%100) < (CHANCE_TO_CONTAMINATE_OTHER_ON_NEIGHBOURG_TILE * 100);
				if(apply_contamination) {
					if(citizens[i].type == FIREFIGHTER) {
						if((rand()%100) < (FIREFIGHTER_CHANCE_TO_RESIST_CONTAMINATION_FROM_OTHERS * 100)) {
							contaminate_citizen(&citizens[i], AMOUNT_OF_CONTAMINATION_TRANSMITED * data->status->contamination);
						}
					}else {
						contaminate_citizen(&citizens[i], AMOUNT_OF_CONTAMINATION_TRANSMITED * data->status->contamination);
					}
				}
			}
		}
	}
}

void tile_contamination(Citizen_data * data, int has_moved) {
	Tile * current_tile;

	current_tile = &data->city->map[data->status->x][data->status->y];

	/* Get contaminated by tile */
	if(has_moved) {
		if(data->status->type == FIREFIGHTER) {
			contaminate_citizen(data->status, HAS_MOVED_GET_CONTAMINATION_FROM_TILE * current_tile->contamination * FIREFIGHTER_TILE_CONTAMINATION_RESISTANCE);
		} else {
			contaminate_citizen(data->status, HAS_MOVED_GET_CONTAMINATION_FROM_TILE * current_tile->contamination);
		}
	} else {
		if(data->status->type == FIREFIGHTER) {
			contaminate_citizen(data->status, DID_NOT_MOVE_GET_CONTAMINATION_FROM_TILE * current_tile->contamination * FIREFIGHTER_TILE_CONTAMINATION_RESISTANCE);
		} else {
			contaminate_citizen(data->status, DID_NOT_MOVE_GET_CONTAMINATION_FROM_TILE * current_tile->contamination);
		}
	}

	/* Contaminate current tile */
	contaminate_tile(current_tile, TILES_CONTAMINATION_RATE * data->status->contamination);
}

int check_if_tile_has_firefighter(Status * citizens, unsigned int x, unsigned int y) {
	int i;
	for(i = 0; i < NB_TOTAL; i++) {
		if(citizens[i].type == FIREFIGHTER && citizens[i].x == x && citizens[i].y == y) {
			return 1;
		}
	}
	return 0;
}

int is_allowed_in(Citizen_data * data, unsigned int x, unsigned int y) {
	Status * citizens;

	citizens = data->city->citizens;
	
	if((x >= CITY_WIDTH) || (y >= CITY_HEIGHT)) {
		return 0;
	}
	if(data->city->map[x][y].nb_citizens == data->city->map[x][y].capacity) {
		return 0;
	}
	if(data->status->type != FIREFIGHTER && data->city->map[x][y].type == FIRESTATION) {
		return check_if_tile_has_firefighter(citizens, x, y);
	}
	if(data->status->type != DOCTOR && data->city->map[x][y].type == HOSPITAL
		&& (data->status->days_spent_in_hospital_healthy > DAYS_ALLOWED_IN_HOSPITAL
		|| !(data->status->is_sick || data->status->type == FIREFIGHTER))) {
		return 0;
	}
	return 1;
}

int max ( int a, int b ) {
	return a > b ? a : b;
}

int chebyshev_distance(int x1, int y1, int x2, int y2) {
	return max(abs(x2 - x1), abs(y2 - y1));
}

Status ** get_citizens_in_tile(unsigned int x, unsigned int y, City * city, int * size) {
	int i, j;
	int max_number;
	Status ** citizen_in_tile;

	max_number = HOUSE_MAX_CAPACITY;
	
	/* get the max value of all the capacities */
	if(max_number < FIRESTATION_MAX_CAPACITY) {
		max_number = FIRESTATION_MAX_CAPACITY;
	}
	if(max_number < HOSPITAL_MAX_CAPACITY) {
		max_number = HOSPITAL_MAX_CAPACITY;
	}
	if(max_number < WASTELAND_MAX_CAPACITY) {
		max_number = WASTELAND_MAX_CAPACITY;
	}
	
	citizen_in_tile = (Status**)malloc(sizeof(Status*) * max_number);
	j = 0;
	for(i = 0; i < NB_TOTAL; i++) {
		if( (city->citizens[i].x == x) &&
			(city->citizens[i].y == y) ) {
			citizen_in_tile[j] = &city->citizens[i];
			j++;
		}
	}
	*size = j;
	return citizen_in_tile;
}