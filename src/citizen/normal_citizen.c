/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file normal_citizen.c
 */

#include "citizen/normal_citizen.h"

extern pthread_cond_t cond;
extern pthread_mutex_t mutex;

pthread_t create_normal_citizen(Citizen_data* data) {
	pthread_t id;
	if(pthread_create(&id, NULL, handle_normal_citizen, (void*)data) < 0) {
		perror("pthread_create()");
	}
	return id;
}

void* handle_normal_citizen(void* args) {
	Citizen_data * data;
	data = (Citizen_data*)args;

	/* TODO add a way to properly close a thread (we need to free his data struct) */

	while(1) {
		play_turn_normal(data);
		pthread_cond_wait(&cond, &mutex);
	}
	return NULL;
}

void play_turn_normal(Citizen_data * data) {
	int has_moved;

	if(data->status->type == NORMAL) {
		if (data->city->map[data->status->x][data->status->y].type == HOSPITAL && !data->status->is_sick) {
			data->status->days_spent_in_hospital_healthy++;
		} else {
			data->status->days_spent_in_hospital_healthy = 0;
		}
		has_moved = move_normal(data);
		/* Dead citizens doesn't contaminate tiles ? */
		tile_contamination(data, has_moved);
		process_sickness(data->status);
	}

	if( (data->status->type == DEAD) ||
		(data->status->type == NORMAL && data->status->is_sick == 1) ) {
		other_citizen_contamination(data);
	}
}
