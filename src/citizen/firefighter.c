/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file firefighter.c
 */

#include "citizen/firefighter.h"

extern pthread_cond_t cond;
extern pthread_mutex_t mutex;

pthread_t create_firefighter(Citizen_data* data) {
	pthread_t id;
	if(pthread_create(&id, NULL, handle_firefighter, (void*)data) < 0) {
		perror("pthread_create()");
	}
	return id;
}

void* handle_firefighter(void* args) {
	Citizen_data * data;
	double pulverisator_capacity;

	pulverisator_capacity = INITIAL_PULVERISATOR_CAPACITY;
	data = (Citizen_data*)args;
	
	if(data->city->map[data->status->x][data->status->y].type != FIRESTATION) {
		pulverisator_capacity /= INITIAL_PULVERISATOR_CAPACITY_REDUCTION_WHEN_OUTSIDE_FIRESTATION;
	}

	while(1) {
		play_turn_firefighter(data, &pulverisator_capacity);
		pthread_cond_wait(&cond, &mutex);
	}
	return NULL;
}

void play_turn_firefighter(Citizen_data * data, double * pulverisator_capacity) {
	int has_moved;

	if(data->status->type == FIREFIGHTER) {
		if (data->city->map[data->status->x][data->status->y].type == HOSPITAL && !data->status->is_sick) {
			data->status->days_spent_in_hospital_healthy++;
		} else {
			data->status->days_spent_in_hospital_healthy = 0;
		}
		has_moved = move_normal(data);
		burn_corpses(data->status->x, data->status->y, data->city);
		if(*pulverisator_capacity > 0) {
			decontaminate_citizens_and_tile(data->status->x, data->status->y, data, pulverisator_capacity);
		}
		/* Dead citizens doesn't contaminate tiles ? */
		tile_contamination(data, has_moved);
		process_sickness(data->status);
		refill_pulverisator(data->status->x, data->status->y, data, pulverisator_capacity);
	}

	if( (data->status->type == DEAD) ||
		(data->status->type == FIREFIGHTER && data->status->is_sick == 1) ) {
		other_citizen_contamination(data);
	}
}

void refill_pulverisator(int x, int y, Citizen_data * data, double * pulverisator_capacity) {
	if(data->city->map[x][y].type == FIRESTATION) {
		*pulverisator_capacity = INITIAL_PULVERISATOR_CAPACITY;
	}
}

void decontaminate_citizens_and_tile(int x, int y, Citizen_data * data, double * pulverisator_capacity) {
	City * city = data->city;
	int most_contaminated_citizen_index = -1;
	double most_contaminated_citizen_contamination;
	int nb_decontaminated = 0;
	int nb_citizen_in_tile, i;
	double can_be_used_this_turn = INITIAL_PULVERISATOR_CAPACITY * PULVERISATOR_USED_EACH_TURN;
	double has_been_used = 0;
	double used_for_tile = 0;
	double capacity_to_use;
	Status ** citizen_in_tile = get_citizens_in_tile(x, y, city, &nb_citizen_in_tile);
	int * has_been_decontaminated = (int*)calloc(nb_citizen_in_tile, sizeof(int));
	
	most_contaminated_citizen_index = 0;
	most_contaminated_citizen_contamination = citizen_in_tile[0]->contamination;

	if(can_be_used_this_turn > *pulverisator_capacity) {
		can_be_used_this_turn = *pulverisator_capacity;
	}

	while(can_be_used_this_turn > 0 && nb_decontaminated < nb_citizen_in_tile) {
		for(i = 0; i < nb_citizen_in_tile; i++) {
			if(!has_been_decontaminated[i]) {
				if(citizen_in_tile[i]->contamination > most_contaminated_citizen_contamination) {
					most_contaminated_citizen_contamination = citizen_in_tile[i]->contamination;
					most_contaminated_citizen_index = i;
				}
			}
		}
		
		if(most_contaminated_citizen_index >= 0) {
			capacity_to_use = citizen_in_tile[most_contaminated_citizen_index]->contamination;

			if(capacity_to_use > MAX_CAPACITY_FOR_A_SINGLE_CITIZEN) {
				capacity_to_use = MAX_CAPACITY_FOR_A_SINGLE_CITIZEN;
			}

			if(capacity_to_use > can_be_used_this_turn) {
				capacity_to_use = can_be_used_this_turn;
			}

			has_been_used += capacity_to_use;
			can_be_used_this_turn -= capacity_to_use;
			decontaminate_citizen(citizen_in_tile[most_contaminated_citizen_index], capacity_to_use);
			has_been_decontaminated[most_contaminated_citizen_index] = 1;
			nb_decontaminated++;
		}
	}
	
	if(can_be_used_this_turn > 0) {
		used_for_tile = (can_be_used_this_turn < city->map[x][y].contamination) ? city->map[x][y].contamination : can_be_used_this_turn;
		decontaminate_tile(&city->map[x][y], used_for_tile);
		has_been_used += used_for_tile;
	}

	*pulverisator_capacity -= has_been_used;
	
	free(has_been_decontaminated);
	free(citizen_in_tile);
}

void burn_corpses(unsigned int x, unsigned int y, City * city) {
	int i;
	Status * citizen;
	int size;
	get_citizens_in_tile(x, y, city, &size);

	for(i = 0; i < NB_TOTAL; i++) {
		citizen = &city->citizens[i];
		if( (citizen->x == x) &&
			(citizen->y == y) ) {
			if(citizen->type == DEAD) {
				citizen->contamination = 0.0;
				citizen->type = BURNED;
			}
		}
	}
}

