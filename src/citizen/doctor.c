/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file doctor.c
 */

#include "citizen/doctor.h"
#include "thread_manager.h"

extern pthread_cond_t cond;
extern pthread_mutex_t mutex;

pthread_t create_doctor(Citizen_data* data) {
	pthread_t id;
	if(pthread_create(&id, NULL, handle_doctor, (void*)data) < 0) {
		perror("pthread_create()");
	}
	return id;
}

void* handle_doctor(void* args) {
	Citizen_data * data;
	int nb_blood_bag;
	int days_spent_outside_hospital;

	data = (Citizen_data*)args;
	nb_blood_bag = NB_BLOOD_BAG_AT_THE_START;
	days_spent_outside_hospital = 0;

	while(1) {
		play_turn_doctor(data, &nb_blood_bag, &days_spent_outside_hospital);
		pthread_cond_wait(&cond, &mutex);
	}
	return NULL;
}

void play_turn_doctor(Citizen_data * data, int * nb_blood_bag, int * days_spent_outside_hospital) {
	int has_moved;
	
	if(data->status->type == DOCTOR) {
		has_moved = move_doctor(data, *days_spent_outside_hospital);
		/* Dead citizens doesn't contaminate tiles ? */
		tile_contamination(data, has_moved);
		process_healing(data, nb_blood_bag, days_spent_outside_hospital);
		process_sickness(data->status);
	}
	if( (data->status->type == DEAD) ||
		(data->status->type == NORMAL && data->status->is_sick == 1) ) {
		other_citizen_contamination(data);
	}
}

void process_healing(Citizen_data * data, int * nb_blood_bag, int * days_spent_outside_hospital) {
	Status * sickest_person;
	
	sickest_person = NULL;
	if (data->status->sickness_duration < 10) {
		sickest_person = find_sickest_citizen(data->city, data->status->x, data->status->y);
	}

	if (data->city->map[data->status->x][data->status->y].type == HOSPITAL) {
		days_spent_outside_hospital = 0;
		(*nb_blood_bag) = NB_BLOOD_BAG_IN_HOSPITAL;
		if(sickest_person && sickest_person->is_sick) {
			heal(sickest_person);
		}
	} else {
		days_spent_outside_hospital++;
		if(*nb_blood_bag && sickest_person && sickest_person->is_sick){
			heal(sickest_person);
			(*nb_blood_bag)--;
		}
	}
}

void heal(Status * status) {
	status->is_sick = 0;
	status->sickness_duration = 0;
}

Status * find_sickest_citizen(City * city, unsigned int x, unsigned int y) {
	int i;
	Status * sickest_person;

	sickest_person = NULL;

	for(i = 0; i < NB_TOTAL; i++) {
		if(city->citizens[i].x == x && city->citizens[i].y == y) {
			if(sickest_person == NULL 
				|| sickest_person->sickness_duration > city->citizens[i].sickness_duration) {
				sickest_person = &(city->citizens[i]);
			}
			
		}
	}

	return sickest_person;
}

int move_doctor(Citizen_data * data, int days_spent_outside_hospital) {
	unsigned int next_x, next_y, do_move;

	do_move = (rand()%100) < (NORMAL_CITIZEN_CHANCE_TO_MOVE * 100);


	if(do_move || !is_allowed_in(data, data->status->x, data->status->y)) {
		do {
			next_x = data->status->x + (rand()%3) - 1;
			next_y = data->status->y + (rand()%3) - 1;
		} while(((next_x == data->status->x) && (next_y == data->status->y))
			|| !is_allowed_in(data, next_x, next_y)
			|| (data->city->map[next_x][next_y].type == HOSPITAL && days_spent_outside_hospital < DAYS_OUTSIDE_UNTIL_GOING_IN_THE_HOSPITAL));

		move_citizen(data->status, data->city, next_x, next_y);
		return 1;
	}
	return 0;
}