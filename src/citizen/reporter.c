/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file reporter.c
 */

#include "citizen/reporter.h"
#include "thread_manager.h"
#include "citizen/citizen_base_functions.h"
#include <mqueue.h>
#include <stdio.h>

extern pthread_cond_t cond;
extern pthread_mutex_t mutex;

pthread_t create_reporter(Citizen_data* data) {
	pthread_t id;
	if(pthread_create(&id, NULL, handle_reporter, (void*)data) < 0) {
		perror("pthread_create()");
	}
	return id;
}

void * handle_reporter(void* args) {
	Citizen_data * data;
	mqd_t mqueue;
	data = (Citizen_data*)args;
	mqueue = mq_open(MQUEUE_NAME, O_WRONLY | O_NONBLOCK);
	while(1) {
		play_turn_reporter(data, mqueue);
		pthread_cond_wait(&cond, &mutex);
	}
	return NULL;
}

void play_turn_reporter(Citizen_data* data, mqd_t mqueue) {
	int has_moved;
	if(data->status->type == REPORTER) {
		if (data->city->map[data->status->x][data->status->y].type == HOSPITAL && !data->status->is_sick) {
			data->status->days_spent_in_hospital_healthy++;
		} else {
			data->status->days_spent_in_hospital_healthy = 0;
		}
		has_moved = move_normal(data);
		tile_contamination(data, has_moved);
		process_sickness(data->status);
	}
	if (data->status->type != DEAD && data->status->type != BURNED) {
		send_reports(data, mqueue);
	}
	if( (data->status->type == DEAD) ||
		(data->status->type == REPORTER && data->status->is_sick == 1) ) {
		other_citizen_contamination(data);
	}
}

void send_dead_citizens(Citizen_data * data, mqd_t mqueue) {
	unsigned int dead_citizens;
	Status status;
	char message[100];
	unsigned int i;
	City * city;
	city = data->city;
	dead_citizens = 0;
	/* TODO : !!! the reporter should not count the number of dead citizens himself */
	for (i = 0; i < NB_TOTAL; ++i) {
		status = city->citizens[i];
		dead_citizens += ((status.type == DEAD) || (status.type == BURNED));
	}
	sprintf(message, "%u %s", dead_citizens, data->status->name);
	mq_send(mqueue, message, sizeof(message), URGENT);
}

void send_city_contamination_level(Citizen_data * data, mqd_t mqueue) {
	unsigned int i;
	unsigned int j;
	char message[100];
	double contamination_level;
	City * city;
	city = data->city;
	for (i = 0; i < CITY_WIDTH; i++) {
		for (j = 0; j < CITY_HEIGHT; j++) {
			contamination_level += city->map[i][j].contamination/(CITY_WIDTH*CITY_HEIGHT); 
		}
	}
	sprintf(message, "%lf %s", contamination_level, data->status->name);
	mq_send(mqueue, message, sizeof(message), IMPORTANT);
}

void send_contaminated_citizens(Citizen_data* data, mqd_t mqueue) {
	unsigned int contaminated_citizens;
	unsigned int i;
	Status status;
	char message[100];
	City * city;
	city = data->city;
	contaminated_citizens = 0;
	for (i = 0; i < NB_TOTAL; ++i) {
		status = city->citizens[i];
		contaminated_citizens += ((status.type != DEAD) && (status.contamination > 0));
	}
	sprintf(message, "%u %s", contaminated_citizens, data->status->name);
	mq_send(mqueue, message, sizeof(message), INFORMATIVE);
}

void send_reporter_contamination_level(Status * my_status, mqd_t mqueue) {
	char message[100];
	sprintf(message, "%lf %s", my_status->contamination, my_status->name);
	mq_send(mqueue, message, sizeof(message), NO_ONE_CARES);
}

void send_reports(Citizen_data* data, mqd_t mqueue) {
	send_dead_citizens(data, mqueue);
	send_city_contamination_level(data, mqueue);
	send_contaminated_citizens(data, mqueue);
	send_reporter_contamination_level(data->status, mqueue);
}
