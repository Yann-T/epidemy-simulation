/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file ncurses_display.c
 */

#include "display/basic_display.h"

void draw_screen(int turn, Statistics stats, City city) {
	/*Uncomment to clear the screen each turn if needed*/
	/*system("clear");*/
	display_city(city);
	display_contamination_map(city);
	display_statistics(stats, turn);
}

void display_city(City city) {
	unsigned int i,j;
	char str[2];
	
	printf("    Alive citizen map\n");

	printf("   ");
	for (int i = 1; i < CITY_WIDTH; i+=2) {
		printf("  %2d", i);
	}
	printf("\n");
	for (i = 0; i < CITY_HEIGHT; ++i) {
		printf(" %2d ", i);
		for (j = 0; j < CITY_WIDTH; ++j) {

			int nb_citizens = get_number_of_living_citizen_on_tile(city, city.map[j][i]);
			sprintf(str, "%d", nb_citizens);
			/* Display only a space when there is no citizen on a tile */
			if(!strcmp("0", str)) {
				str[0] = ' ';
			}
			printf("%s%2s%s", get_color_prefix_from_building_type(city.map[j][i].type), str, COLOR_RESET);
		}
		printf("\n");
	}
}


void display_contamination_map(City city) {
	unsigned int i,j;

	printf("    Contamination Map\n");

	printf("   ");
	for (int i = 1; i < CITY_WIDTH; i+=2) {
		printf("  %2d", i);
	}
	printf("\n");
	for (i = 0; i < CITY_HEIGHT; ++i) {
		printf(" %2d ", i);
		for (j = 0; j < CITY_WIDTH; ++j) {
			printf("%s  %s", get_color_prefix_from_contamination_level(city.map[j][i].contamination),
								"\x1b[0m");
		}
		printf("\n");
	}
}

char* get_color_prefix_from_building_type(Building_type type) {
	switch(type) {
		case WASTELAND : return WASTELAND_COLOR;
		case HOUSE : return HOUSE_COLOR;
		case HOSPITAL : return HOSPITAL_COLOR;
		case FIRESTATION : return FIRESTATION_COLOR;
		default : return COLOR_RESET;
	}
}

char* get_color_prefix_from_contamination_level(double contamination) {
	if(contamination > 0.45) {
		return VERY_HIGH_CONTAMINATION_COLOR;
	}
	if(contamination > 0.3) {
		return HIGH_CONTAMINATION_COLOR;
	}
	if(contamination > 0.15) {
		return AVERAGE_CONTAMINATION_COLOR;
	}
	if(contamination > 0.01) {
		return SLIGHT_CONTAMINATION_COLOR;
	}
	return NO_CONTAMINATION_COLOR;
}


void display_statistics(Statistics stats, int turn) {
	printf("== Turn %d ==\n", turn);
	printf("- Healthy citizen(s): %d\n", stats.healthy);
	printf("- Sick citizen(s): %d\n", stats.sick);
	printf("- Dead citizen(s): %d\n", stats.dead);
	printf("- Burned citizen(s): %d\n", stats.burned);
	fflush(stdout);
}