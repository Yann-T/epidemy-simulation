/*
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */ 

/**
 * @author Benjamin SAMUTH <bsamuth@ecole.ensicaen.fr>
 * @author Yann TRUPOT <ytrupot@ecole.ensicaen.fr>
 * @author Erwan CARIOU <ecariou@ecole.ensicaen.fr>
 * @version 1.0.0
 */

/**
 * @file ncurses_display.c
 */

#include "display/ncurses_display.h"

void draw_screen_ncurses(int turn, Statistics stats, City city, int offset_x, int offset_y) {
	clear();
	display_city_ncurses(city, offset_x + 1, offset_y);
	display_contamination_map_ncurses(city, offset_x + (CITY_WIDTH * 2 + 6), offset_y);
	display_statistics_ncurses(stats, turn, offset_x + 5, offset_y + (CITY_HEIGHT + 5));
	move(offset_y + (CITY_HEIGHT + 10), offset_x);
	refresh();
}

void display_city_ncurses(City city, int offset_x, int offset_y) {
	unsigned int i,j;
	char str[2];

	move(offset_y++, offset_x + 4);
	
	printw("Alive citizen map");
	offset_y += 2;
	move(offset_y++, offset_x + 4);
	for (int i = 1; i < CITY_WIDTH; i+=2) {
		printw("  %2d", i);
	}
	move(offset_y++, offset_x);
	for (i = 0; i < CITY_HEIGHT; ++i) {
		printw(" %2d ", i);
		for (j = 0; j < CITY_WIDTH; ++j) {

			int nb_citizens = get_number_of_living_citizen_on_tile(city, city.map[j][i]);
			sprintf(str, "%d", nb_citizens);
			/* Display only a space when there is no citizen on a tile */
			if(!strcmp("0", str)) {
				str[0] = ' ';
			}
			attron(COLOR_PAIR(city.map[j][i].type + 1));
			printw("%2s", str);
			attroff(COLOR_PAIR(city.map[j][i].type + 1));
		}
		move( offset_y++, offset_x);
	}
}

void display_contamination_map_ncurses(City city, int offset_x, int offset_y) {
	unsigned int i,j;

	move(offset_y++, offset_x + 4);
	
	printw("Tiles contamination map");
	offset_y += 2;
	move(offset_y++, offset_x + 4);
	for (int i = 1; i < CITY_WIDTH; i+=2) {
		printw("  %2d", i);
	}
	move(offset_y++, offset_x);
	for (i = 0; i < CITY_HEIGHT; ++i) {
		printw(" %2d ", i);
		for (j = 0; j < CITY_WIDTH; ++j) {
			attron(COLOR_PAIR(get_color_index_from_contamination_level(city.map[j][i].contamination)));
			printw("  ");
			attroff(COLOR_PAIR(get_color_index_from_contamination_level(city.map[j][i].contamination)));
		}
		move(offset_y++, offset_x);
	}
}

int get_color_index_from_contamination_level(double contamination) {
	if(contamination > 0.45) {
		return VERY_HIGH_CONTAMINATION_COLOR;
	}
	if(contamination > 0.3) {
		return HIGH_CONTAMINATION_COLOR;
	}
	if(contamination > 0.15) {
		return AVERAGE_CONTAMINATION_COLOR;
	}
	if(contamination > 0.01) {
		return SLIGHT_CONTAMINATION_COLOR;
	}
	return NO_CONTAMINATION_COLOR;
}

void init_ncurses_colors() {
	start_color();
	
	init_pair(WASTELAND_COLOR, COLOR_BLACK, COLOR_YELLOW);
	init_pair(HOUSE_COLOR, COLOR_BLACK, COLOR_WHITE);
	init_pair(HOSPITAL_COLOR, COLOR_WHITE, COLOR_BLUE);
	init_pair(FIRESTATION_COLOR, COLOR_WHITE, COLOR_RED);
	init_pair(NO_CONTAMINATION_COLOR, COLOR_WHITE, COLOR_WHITE);
	init_pair(SLIGHT_CONTAMINATION_COLOR, COLOR_WHITE, COLOR_GREEN);
	init_pair(AVERAGE_CONTAMINATION_COLOR, COLOR_WHITE, COLOR_YELLOW);
	init_pair(HIGH_CONTAMINATION_COLOR, COLOR_WHITE, COLOR_RED);
	init_pair(VERY_HIGH_CONTAMINATION_COLOR, COLOR_WHITE, COLOR_MAGENTA);
	init_pair(HEALTHY_CITIZEN_TEXT_COLOR, COLOR_GREEN, COLOR_BLACK);
	init_pair(SICK_CITIZEN_TEXT_COLOR, COLOR_YELLOW, COLOR_BLACK);
	init_pair(DEAD_CITIZEN_TEXT_COLOR, COLOR_RED, COLOR_BLACK);
	init_pair(BURNED_CITIZEN_TEXT_COLOR, COLOR_WHITE, COLOR_BLACK);
}

void display_statistics_ncurses(Statistics stats, int turn, int offset_x, int offset_y) {
	move(offset_y++, offset_x);
	printw("== Turn %d ==", turn);
	move(offset_y++, offset_x);
	printw("- Healthy citizen(s): ");
	attron(COLOR_PAIR(HEALTHY_CITIZEN_TEXT_COLOR));
	printw("%d", stats.healthy);
	attroff(COLOR_PAIR(HEALTHY_CITIZEN_TEXT_COLOR));
	move(offset_y++, offset_x);
	printw("- Sick citizen(s): ", stats.sick);
	attron(COLOR_PAIR(SICK_CITIZEN_TEXT_COLOR));
	printw("%d", stats.sick);
	attroff(COLOR_PAIR(SICK_CITIZEN_TEXT_COLOR));
	move(offset_y++, offset_x);
	printw("- Dead citizen(s): ", stats.dead);
	attron(COLOR_PAIR(DEAD_CITIZEN_TEXT_COLOR));
	printw("%d", stats.dead);
	attroff(COLOR_PAIR(DEAD_CITIZEN_TEXT_COLOR));
	move(offset_y++, offset_x);
	printw("- Burned citizen(s): ");
	attron(COLOR_PAIR(BURNED_CITIZEN_TEXT_COLOR));
	printw("%d", stats.burned);
	attroff(COLOR_PAIR(BURNED_CITIZEN_TEXT_COLOR));
	move(offset_y++, offset_x);
	fflush(stdout);
}