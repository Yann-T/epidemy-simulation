CC=gcc
CFLAGS=-Wall -Wextra -pedantic -c
INC=./inc/
SRC=./src/
OBJ=./obj/
BIN=./bin/
MISC=./misc/
CPPFLAGS=-I$(INC)

OBJS_EPIDEMIC_SIM=$(OBJ)epidemic_sim.o $(OBJ)city.o $(OBJ)epidemic_sim_exec.o $(OBJ)epidemic_evolution.o $(OBJ)display/ncurses_display.o $(OBJ)display/basic_display.o
EXEC_EPIDEMIC_SIM=$(BIN)epidemic_sim
LDFLAGS_EPIDEMIC_SIM= -lncurses -lrt

OBJS_CITIZEN=$(OBJ)thread_manager.o $(OBJ)citizen_manager.o $(OBJ)citizen/citizen_base_functions.o $(OBJ)city.o $(OBJ)citizen/normal_citizen.o $(OBJ)name_picker.o $(OBJ)tiles.o $(OBJ)citizen/reporter.o $(OBJ)citizen/doctor.o $(OBJ)citizen/firefighter.o
EXEC_CITIZEN=$(BIN)citizen_manager
LDFLAGS_CITIZEN_MANAGER=-lpthread -lrt -lm

OBJS_PRESS=$(OBJ)press_agency.o
EXEC_PRESS=$(BIN)press_agency
LDFLAGS_PRESS=-lrt

EXEC=$(EXEC_CITIZEN) $(EXEC_EPIDEMIC_SIM) $(BIN)timer $(EXEC_PRESS)

.PHONY : all init clean distclean

all: init $(EXEC) 

$(EXEC_CITIZEN): $(OBJS_CITIZEN)
	$(CC) $^ -o $@ $(LDFLAGS_CITIZEN_MANAGER)

$(EXEC_EPIDEMIC_SIM): $(OBJS_EPIDEMIC_SIM)
	$(CC) $^ -o $@ $(LDFLAGS_EPIDEMIC_SIM)

$(EXEC_PRESS): $(OBJS_PRESS)
	$(CC) $^ -o $@ $(LDFLAGS_PRESS)

$(BIN)timer: $(OBJ)timer.o
	$(CC) $^ -o $@

$(OBJ)%.o: $(SRC)%.c
	$(CC) $(CPPFLAGS) $(CFLAGS) \
		$< -o $@

$(BIN): 
	mkdir -p $(BIN)

$(OBJ): $(OBJ)citizen $(OBJ)display
	mkdir -p $(OBJ);

$(OBJ)citizen:
	mkdir -p $(OBJ)citizen

$(OBJ)display:
	mkdir -p $(OBJ)display

init: $(BIN) $(OBJ)

clean:
	rm -fr 	$(SRC)*~ $(SRC)citizen/*~ $(SRC)display/*~ \
		$(INC)*~ $(INC)citizen/*~ $(INC)display/*~ \
		$(OBJ)* \
		$(MISC)gnuplot/evolution.txt

distclean: clean
	rm -f 	$(BIN)*
